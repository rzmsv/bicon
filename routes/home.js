const express = require("express");
const router = express.Router();
const getHome = require("../controller/get");

router.get("/",getHome.home);

module.exports = router;