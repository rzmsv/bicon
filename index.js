const express = require("express");
const dotENV = require("dotenv").config(); // for config ENV file...
const http = require("http");
const middlewares = require("./middlwares/middlewares");
const homePage = require("./routes/home")



const app = express();
const server = http.createServer(app);

//Middlewares
middlewares.middlewares(app);
// Routes
app.use(homePage)


const port = process.env.PORT
const host = process.env.HOST
server.listen(port,()=>{
     console.log(`connect to port http://${host}:${port}`)
})