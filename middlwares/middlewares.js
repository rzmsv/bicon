//-------------------------///
// ALL MIDLEWARES ARE HERE ...
//-------------------------///

const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");
const favicon = require('serve-favicon');


exports.middlewares = (app)=>{
     // app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
     app.use(express.static(path.join(__dirname,"public")));
     app.use(express.json());
     app.use(bodyParser.json());
     app.use(bodyParser.urlencoded({extended:false}));
     app.use(cors());
     app.use(morgan("common"));
     app.use(helmet());
     
}